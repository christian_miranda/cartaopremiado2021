@extends('site.layout.site')

@section('content')
	<div class="fh5co-loader"></div>

	<div id="page">
        @include ('site.partials.header_interno')
        @include ('site.partials.menu-responsivo-interno')
        @include ('site.partials.banner-principal')
        @include ('site.partials.banner-mob')

        <style type="text/css">

            .services {
                width: 95%;
                margin: 0 auto;
                margin-bottom: 40px;
                position: relative;
                -webkit-transition: 0.3s;
                -o-transition: 0.3s;
                transition: 0.3s;
            }

            .services h4 {
                font-size: 20px;
                font-weight: 400;
                color: #6d6e71;
                margin-top: 20px;
                line-height: 30px;
                margin-bottom: 30px;
            }
             
            .cooperativas h4 {
                margin-bottom: 1px !important;
                margin-top: 5px !important;
                font-size: 16px;
                text-align: left;
                color: #fff;
            }

            .services h3 {
                font-size: 20px;
                font-weight: 400;
                color: #6d6e71;
                margin-top: 40px;
                line-height: 30px;
            }

            .cooperativas h3 {
                margin-top: 5px;
                margin-bottom: 40px;
                color: #fff;
            }
        </style>
        
        <div id="cooperativas-participantes"></div>
        <div class="" style="background-color:#42a0ab;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-10 offset-md-1" style="padding: 50px 0px 50px 0px;">
                        <h1 class="h1-title"><img src="{{asset('site/images/txt_cooperativas.png')}}" alt="" class="img-responsive animate-box fadeInUp animated-fast" style="width:310px;"></h1>
                    </div>
                </div>
                <div class="row justify-content-center">		
                    <div class="col-md-12 text-center animate-box">
                        <div class="services cooperativas">
                            <div class="desc">
                                <h3>Acesse o regulamento para mais informações.</h3>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB AGROCREDI </h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB ARACREDI</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB BELCREDI</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CARLOS CHAGAS</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CENTRO UNIÃO</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CENTRO-SUL MINEIRO</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB COOPACREDI</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB COOPEROSA</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB COPERSUL</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDCAM</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDCOOPER</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIAGRO</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIALP</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIARA</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIBAM</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIBELO</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIBOM</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDICAF</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDICAMPINA</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDICAMPO</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDICAPI</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDICARMO</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDICARPA</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDICOOP</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDICOPE</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIESMERALDAS</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIFIEMG</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIFOR</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIGUAPÉ</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIJEQUITINHONHA</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDILIVRE</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDILUZ</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIMAC</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIMATA</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIMIL</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIMONTE</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDINDAIÁ</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDINOR</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDINORTE</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDINOSSO</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDINTER</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIOESTE</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIPATOS</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIPEL</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIPÉU</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIPIMENTA</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIPINHO</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIPRATA</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIRAMA</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIRIODOCE</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDISALES</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDISETE</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDISG</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDISUCESSO</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDISUDESTE</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDITAMA</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDITIROS</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIUNA</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIVAG</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIVALE</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIVAR</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIVASS</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIVAZ</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIVERDE</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB CREDIVERTENTES</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB FRUTAL</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB GUARANICREDI</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB ITAPAGIPE</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB MONTECREDI</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB NOROESTE DE MINAS</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB NOSSOCRÉDITO</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB PROFISSIONAIS DA SAUDE</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB SACRAMENTO</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB UBERABA</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB UNIÃO</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB UNIÃO CENTRAL</h4>
                                <h4 class="col-md-4 offset-md-4 animate-box fadeInUp animated-fast">SICOOB UNIÃO CENTRO-OESTE</h4>
                            </div>
                        </div>
                    </div>						
                </div>
            </div>
        </div>



    @include ('site.partials.form-contato--mob')
    @include ('site.partials.footer')


    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            setTimeout(function () {
                scrollToDiv('#cooperativas-participantes');
            }, 600);
        });
    </script>
@endpush