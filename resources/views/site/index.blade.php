@extends('site.layout.site')

@section('content')

    @include ('site.partials.header')
    @include ('site.partials.menu-responsivo')
    @include ('site.partials.banner-principal')
    @include ('site.partials.banner-mob')        
    @include ('site.partials.trevo')    
    @include ('site.partials.numeros-pesquisa')
    @include ('site.partials.sorteios')
    @include ('site.partials.regulamento')
    @include ('site.partials.form-contato--mob')
    @include ('site.partials.footer')

@endsection

@push('scripts')
    @if(Session::has('lk_goto'))
        <script type="text/javascript">
            $(document).ready(function() {
                setTimeout(function () {
                    scrollToDiv('{{Session::get('lk_goto')}}');
                }, 600);
            });
        </script>
    @else
        <script type="text/javascript">
            $(document).ready(function() {
                $('html, body').animate({
                    scrollTop: $('#gotohome').offset().top - 70
                }, 500, 'easeInOutExpo');
                
                return false;
            });
        </script>
    @endif
@endpush
