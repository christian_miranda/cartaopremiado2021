<div id="como_participar"></div>
<section class="sobre inner">
    <div class="container">
        <div class="sobre-box__content">
            <img src="{{ asset('site/images/seta1.png') }}" alt="" class="img-fluid">
            <h1 class="title"> COMO <strong>PARTICIPAR</strong> </h1>
        </div>
        <div class="row align-items-center">
            <div class="col-md-4 offset-md-2">
                <div class="sobre-box__valor">
                    <div class="sobre-box__valor--reais">
                        100 reais
                        <small>em compras na função crédito</small>
                    </div>
                    <div class="sobre-box__valor--sorte">
                        1 número da sorte
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sobre-box__maquininha">
                    <div class="">
                        <img src="{{ asset('site/images/maquininha.png') }}" alt="">
                    </div>
                    <div class="">
                        Na maquininha Sipag sorte em dobro.
                    </div>
                </div>
            </div>
        </div>


        <div class="text-center sobre-box__content--desk">
            <img src="{{ asset('site/images/hand.png') }}" alt="" class="img-fluid">
        </div>

        <div class="sobre-box__participantes--mob">
            <div class="sobre-box__participantes">
                Para participar, não precisa se cadastrar.
                É só usar o seu Sicoobcard na função crédito. 
                Acesse o regulamento para mais informações.
            </div>
            <div class="sobre-box__cruzados">
                <div class="sobre-box__cruzados--img">
                    <img src="{{ asset('site/images/dedos-cruzados.png') }}" alt="">
                </div>
                <div class="">
                    <h3> Cartões participantes </h3>
                    Sicoobcard:  Cabal Clássico, Cabal Essencial, Cabal Empresarial, Cabal Gold, Mastercard Clássico, Mastercard Empresarial, Mastercard Executivo, Mastercard Corporativo, Mastercard Gold, Mastercard Platinum, Mastercard Black, Mastercard Black Merit, Visa Clássico, Visa Gold, Visa Platinum, Visa Executivo, Vooz e demais cartões de crédito que venham a ser lançados comercialmente pelo Bancoob durante a promoção.
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="sobre-box__confira">
                    <div class="sobre-box__interno">
                        Campanha exclusiva para associados das
                        cooperativas filiadas ao Sicoob Central Crediminas .
                    </div>
                <a href="{{ route('cooperatives') }}" class="btn btn-cooperativas">
                        Confira as cooperativas participantes
                    </a>
                </div>
            </div>
        </div>


    </div>
</section>