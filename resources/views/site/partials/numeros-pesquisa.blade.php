<div id="numeros_da_sorte"></div>
<section class="pesquisa-num inner">
    <div class="container">
        <header class="header-pesquisa">
            <h4 class="title">
                Números da sorte
            </h4>
            <img src="{{ asset('site/images/seta3.png') }}" alt="">
        </header>
        <div class="pesquisa-num__form">


            <div class="row">
                <div class="col-md-4 offset-md-5">
                    <h5>Consulte seus Números da sorte</h5>
                    <form  action="{{route('cupons.consult')}}" method="post" class="pesquisa-num__form--formulario">
                        <div class="form-group">
                       
                            <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                            <input type="text" class="form-control search-input" name="txt_document" id="txt_cupom" onkeypress="onlyNumber('#txt_cupom');" placeholder="Digite seu CPF ou CNPJ para consultar seus números da sorte:">
                            <div class="text-right" style="padding-top: 20px;">
                                <!-- <input type="submit" class="btn btn-dark" id="btn_search_cupom" value=""> -->
                                <button type="submit" class="btn btn-dark" id="btn_search_cupom">Buscar</button>
                            </div>
                        
                        </div>
                        <!-- <div class="text-right">
                            <button class="btn btn-dark">Buscar</button>
                        </div> -->
                    </form>
                </div>
            </div>
        </div>
        
        <div class="row pesquisa-num__trevo">
            <div class="col-xs-6 col-md-4 offset-md-2 text-center">
                <img src="{{ asset('site/images/trevo.png') }}" alt="Trevo de 4 folhas" class="img-fluid">
            </div>
            <div class="col-xs-6 col-md-4">
                <p>
                    <strong> Compras de dezembro </strong>
                    Números da sorte divulgados dia 17 de janeiro.
                </p>
                        
                <p>
                    <strong>Compras de janeiro </strong>
                    Números da sorte divulgados dia 14 de fevereiro.
                </p>
                        
                <p>
                    <strong>Compras de Fevereiro</strong>
                    Números da sorte divulgados dia 16 de março.
                </p>
            </div>
        </div>

    </div>
</section>