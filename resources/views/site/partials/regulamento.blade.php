<section class="regulamentos inner">
    <div class="container regulamentos__content">
        <h3 class="title regulamentos__title"> Regulamento </h3>
        <a href="{{ route('regulation') }}">
            <img src=" {{ asset('site/images/clique-regulamento-desk.png') }}" alt="" class="img-fluid">
        </a>
    </div>
    <div class="container regulamentos__mob">
        <h3 class="title regulamentos__title"> Regulamento </h3>
        <a href="{{ route('regulation') }}" class="">
        <img src=" {{ asset('site/images/clique-regulamento-mob.png') }}" alt="Regulamento" class="img-fluid">
        </a>
    </div>
</section>