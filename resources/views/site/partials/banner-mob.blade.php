<section class="banner-mob">
  <div class="banner-mob__principal">
      <img src="{{ asset('site/images/banner.jpg') }}" alt="" class="img-fluid">
  </div>
  <div class="banner-mob__video">
      <div class="container">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/SYSS86SG278" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <p class="banner-mob__disclaimer">
              Promoção válida de 1/12/19 a 29/2/20 - Certificado de Autorização SECAP/ME n° 04.006606/2019.
          </p>
      </div>
  </div>
</section>