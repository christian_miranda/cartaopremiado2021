<header id="menu-site" class="header fixed-top">
    <div class="container">
        <div class="header-box">
            <div class="header-box__logo">
                <a href="">
                    <img src="{{ asset('site/images/logo.png') }}" alt="Sicoob faça parte">
                </a>
            </div>
            <div class="header-box__navigation">
                <!-- Entrara o menu desktop -->
                <nav>
                    <ul class="header-box__navigation--nav">
                    <li><a href="{{ route('home') }}" class="header-box__navigation--nav-item">Home</a></li>
                        <li>
                            <a class="header-box__navigation--nav-item" href="javascript:void(0);" onclick="scrollToDiv('#como_participar');">Como participar</a>
                        </li>
                        <li>
                            <a class="header-box__navigation--nav-item" href="javascript:void(0);" onclick="scrollToDiv('#numeros_da_sorte');">Números da sorte</a>
                        </li>
                        <li>
                            <a class="header-box__navigation--nav-item" href="javascript:void(0);" onclick="scrollToDiv('#sorteio');">Sorteios</a>
                        </li>
                        <li>
                            <a href="{{ route('regulation') }}" class="header-box__navigation--nav-item">Regulamento</a>
                        </li>
                        <li>
                            <a class="header-box__navigation--nav-item" href="javascript:void(0);" onclick="scrollToDiv('#contato');">Contato</a>
                        </li>
                    <li>
                        <a href="{{ route('cooperatives') }}" class="header-box__navigation--nav-item">Cooperativas participantes</a>
                    </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="btn-menu-responsive d-block d-lg-none">
            <div class="icon-bar"></div>
        </div>
    </div>
</header>