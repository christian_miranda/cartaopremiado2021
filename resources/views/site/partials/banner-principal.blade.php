<section class="banner-principal">
    <img src="{{ asset('site/images/banner-principal.jpg') }}" alt="" class="img-fluid">
</section>
<section class="banner-principal__video inner">
    <div class="container">
        <h3 class="title banner-principal__video--title" >
            COMPRA VAI, COMPRA VEM E SUAS CHANCES
            DE GANHAR um carro Zero SÓ AUMENTAM.
        </h3>
        <div class="embed-responsive embed-responsive-16by9">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/SYSS86SG278" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</section>