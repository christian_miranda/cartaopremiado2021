<div id="contato"></div>
<section class="contato inner">
    <div class="container">
        <h6 class="title text-center"> Contato </h6>
        <form action="{{route('submitform')}}" method="post">

            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" name="txt_name" id="txt_name" class="form-control" placeholder="Nome"  value="{{old('txt_name')}}" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" name="txt_email" id="txt_email" class="form-control" placeholder="E-mail"  value="{{old('txt_email')}}" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" name="txt_phone" id="txt_phone" class="form-control" placeholder="Telefone" value="{{old('txt_phone')}}" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <select name="txt_uf" id="txt_uf" class="form-control" required>
                            <option value="MG">Minas Gerais</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <select name="txt_coop" id="txt_coop" class="form-control" required>
                            <option value="Crediminas">SICOOB AGROCREDI </option>
                            <option value="Crediminas">SICOOB ARACREDI</option>
                            <option value="Crediminas">SICOOB BELCREDI</option>
                            <option value="Crediminas">SICOOB CARLOS CHAGAS</option>
                            <option value="Crediminas">SICOOB CENTRO UNIÃO</option>
                            <option value="Crediminas">SICOOB CENTRO-SUL MINEIRO</option>
                            <option value="Crediminas">SICOOB COOPACREDI</option>
                            <option value="Crediminas">SICOOB COOPEROSA</option>
                            <option value="Crediminas">SICOOB COPERSUL</option>
                            <option value="Crediminas">SICOOB CREDCAM</option>
                            <option value="Crediminas">SICOOB CREDCOOPER</option>
                            <option value="Crediminas">SICOOB CREDIAGRO</option>
                            <option value="Crediminas">SICOOB CREDIALP</option>
                            <option value="Crediminas">SICOOB CREDIARA</option>
                            <option value="Crediminas">SICOOB CREDIBAM</option>
                            <option value="Crediminas">SICOOB CREDIBELO</option>
                            <option value="Crediminas">SICOOB CREDIBOM</option>
                            <option value="Crediminas">SICOOB CREDICAF</option>
                            <option value="Crediminas">SICOOB CREDICAMPINA</option>
                            <option value="Crediminas">SICOOB CREDICAMPO</option>
                            <option value="Crediminas">SICOOB CREDICAPI</option>
                            <option value="Crediminas">SICOOB CREDICARMO</option>
                            <option value="Crediminas">SICOOB CREDICARPA</option>
                            <option value="Crediminas">SICOOB CREDICOOP</option>
                            <option value="Crediminas">SICOOB CREDICOPE</option>
                            <option value="Crediminas">SICOOB CREDIESMERALDAS</option>
                            <option value="Crediminas">SICOOB CREDIFIEMG</option>
                            <option value="Crediminas">SICOOB CREDIFOR</option>
                            <option value="Crediminas">SICOOB CREDIGUAPÉ</option>
                            <option value="Crediminas">SICOOB CREDIJEQUITINHONHA</option>
                            <option value="Crediminas">SICOOB CREDILIVRE</option>
                            <option value="Crediminas">SICOOB CREDILUZ</option>
                            <option value="Crediminas">SICOOB CREDIMAC</option>
                            <option value="Crediminas">SICOOB CREDIMATA</option>
                            <option value="Crediminas">SICOOB CREDIMIL</option>
                            <option value="Crediminas">SICOOB CREDIMONTE</option>
                            <option value="Crediminas">SICOOB CREDINDAIÁ</option>
                            <option value="Crediminas">SICOOB CREDINOR</option>
                            <option value="Crediminas">SICOOB CREDINORTE</option>
                            <option value="Crediminas">SICOOB CREDINOSSO</option>
                            <option value="Crediminas">SICOOB CREDINTER</option>
                            <option value="Crediminas">SICOOB CREDIOESTE</option>
                            <option value="Crediminas">SICOOB CREDIPATOS</option>
                            <option value="Crediminas">SICOOB CREDIPEL</option>
                            <option value="Crediminas">SICOOB CREDIPÉU</option>
                            <option value="Crediminas">SICOOB CREDIPIMENTA</option>
                            <option value="Crediminas">SICOOB CREDIPINHO</option>
                            <option value="Crediminas">SICOOB CREDIPRATA</option>
                            <option value="Crediminas">SICOOB CREDIRAMA</option>
                            <option value="Crediminas">SICOOB CREDIRIODOCE</option>
                            <option value="Crediminas">SICOOB CREDISALES</option>
                            <option value="Crediminas">SICOOB CREDISETE</option>
                            <option value="Crediminas">SICOOB CREDISG</option>
                            <option value="Crediminas">SICOOB CREDISUCESSO</option>
                            <option value="Crediminas">SICOOB CREDISUDESTE</option>
                            <option value="Crediminas">SICOOB CREDITAMA</option>
                            <option value="Crediminas">SICOOB CREDITIROS</option>
                            <option value="Crediminas">SICOOB CREDIUNA</option>
                            <option value="Crediminas">SICOOB CREDIVAG</option>
                            <option value="Crediminas">SICOOB CREDIVALE</option>
                            <option value="Crediminas">SICOOB CREDIVAR</option>
                            <option value="Crediminas">SICOOB CREDIVASS</option>
                            <option value="Crediminas">SICOOB CREDIVAZ</option>
                            <option value="Crediminas">SICOOB CREDIVERDE</option>
                            <option value="Crediminas">SICOOB CREDIVERTENTES</option>
                            <option value="Crediminas">SICOOB FRUTAL</option>
                            <option value="Crediminas">SICOOB GUARANICREDI</option>
                            <option value="Crediminas">SICOOB ITAPAGIPE</option>
                            <option value="Crediminas">SICOOB MONTECREDI</option>
                            <option value="Crediminas">SICOOB NOROESTE DE MINAS</option>
                            <option value="Crediminas">SICOOB NOSSOCRÉDITO</option>
                            <option value="Crediminas">SICOOB PROFISSIONAIS DA SAUDE</option>
                            <option value="Crediminas">SICOOB SACRAMENTO</option>
                            <option value="Crediminas">SICOOB UBERABA</option>
                            <option value="Crediminas">SICOOB UNIÃO</option>
                            <option value="Crediminas">SICOOB UNIÃO CENTRAL</option>
                            <option value="Crediminas">SICOOB UNIÃO CENTRO-OESTE</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <textarea name="txt_message" id="txt_message" placeholder="Mensagem" cols="30" rows="10" class="form-control" required>{{old('txt_message')}}</textarea>
            </div>
            <div class="row">
                <div class="col-md-4 offset-md-4">
                    <button class="btn btn-contato btn-block"> Enviar </button>
                </div>
            </div>

            @if(Session::has('messageform'))
                <div id="message-send" class="<?php echo  \Session::get('messageclass'); ?>"><?php echo  \Session::get('messageform'); ?></div>
            @else
                <div id="message-send"></div>
            @endif

        </form>
    </div>
</section>