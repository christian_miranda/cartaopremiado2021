<nav>
    <div class="body-menu-responsive">
        <ul class="lista-menu">
            <li>
                <a href="{{ route('home') }}" class="header-box__navigation--nav-item">Home</a>
            </li>
            <li>
                <a href="{{url('cartapremiado')}}/como_participar" class="header-box__navigation--nav-item">Como participar</a>
            </li>
            <li>
                <a href="{{url('cartapremiado')}}/numeros_da_sorte" class="header-box__navigation--nav-item">Números da sorte</a>
            </li>
            <li>
                <a href="{{url('cartapremiado')}}/sorteio" class="header-box__navigation--nav-item">Sorteios</a>
            </li>
            <li>
                <a href="{{ route('regulation') }}" class="header-box__navigation--nav-item">Regulamento</a>
            </li>
            <li>
                <a href="{{url('cartapremiado')}}/contato" class="header-box__navigation--nav-item">Contato</a>
            </li>
        <li>
            <a href="{{ route('cooperatives') }}" class="header-box__navigation--nav-item">Cooperativas participantes</a>
        </li>
        </ul>
    </div>
</nav>