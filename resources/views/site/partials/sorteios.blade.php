<div id="sorteio"></div>
<section class="sorteios inner">
    <div class="container">
        <h6 class="title"> Sorteios </h6>

        <div class="row">
            <div class="col-md-6">
                <div class="row ganhadores">
                    <div class="col-6 title-grid">Nome</div>
                    <div class="col-6 title-grid">Número da sorte</div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                            Guilherme D'Angelis Eleutério
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                            Série 211 - Nº 99818
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row ganhadores">
                    <div class="col-6 title-grid">Cooperativa</div>
                    <div class="col-6 title-grid">Data</div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                            Sicoob Credinor 
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                            22/01/2020
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row ganhadores">
                    <div class="col-6 title-grid d-block d-md-none">Nome</div>
                    <div class="col-6 title-grid d-block d-md-none">Número da sorte</div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                            Pedro Marciano Gomes
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                            Série 028 - Nº 80.948
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row ganhadores">
                    <div class="col-6 title-grid d-block d-md-none">Cooperativa</div>
                    <div class="col-6 title-grid d-block d-md-none">Data</div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                            Sicoob Credijequitinhonha
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                            19/02/2020
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row ganhadores">
                    <div class="col-6 title-grid d-block d-md-none">Nome</div>
                    <div class="col-6 title-grid d-block d-md-none">Número da sorte</div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                            Gender José Brigagão Pinheiro de Alcantara
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                            Série 125 - Nº 14.298
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row ganhadores">
                    <div class="col-6 title-grid d-block d-md-none">Cooperativa</div>
                    <div class="col-6 title-grid d-block d-md-none">Data</div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                        Sicoob Nossocrédito
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                        18/03/2020
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row ganhadores">
                    <div class="col-6 title-grid d-block d-md-none">Nome</div>
                    <div class="col-6 title-grid d-block d-md-none">Número da sorte</div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                            Reinaldo Vieira Jacinto
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                        Série 155 - Nº 14.299
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row ganhadores">
                    <div class="col-6 title-grid d-block d-md-none">Cooperativa</div>
                    <div class="col-6 title-grid d-block d-md-none">Data</div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                        Sicoob Coopacredi
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                        18/03/2020
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row ganhadores">
                    <div class="col-6 title-grid d-block d-md-none">Nome</div>
                    <div class="col-6 title-grid d-block d-md-none">Número da sorte</div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                            Andréa Silva Gonçalves Dias
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                        Série 185 - Nº 14.305
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row ganhadores">
                    <div class="col-6 title-grid d-block d-md-none">Cooperativa</div>
                    <div class="col-6 title-grid d-block d-md-none">Data</div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                        Sicoob Credirama
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="dados-ganhador">
                        18/03/2020
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>