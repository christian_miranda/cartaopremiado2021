<header id="menu-site" class="header fixed-top">
    <div class="container">
        <div class="header-box">
            <div class="header-box__logo">
                <a href="">
                    <img src="{{ asset('site/images/logo.png') }}" alt="Sicoob faça parte">
                </a>
            </div>
            <div class="header-box__navigation">
                <!-- Entrara o menu desktop -->
                <nav>
                    <ul class="header-box__navigation--nav">
                    
                        <li>
                            <a href="{{url('cartapremiado')}}/como_participar" class="header-box__navigation--nav-item">Como participar</a>
                        </li>
                        <li>
                            <a href="{{url('cartapremiado')}}/numeros_da_sorte" class="header-box__navigation--nav-item">Números da sorte</a>
                        </li>
                        <li>
                            <a href="{{url('cartapremiado')}}/sorteio" class="header-box__navigation--nav-item">Sorteios</a>
                        </li>
                        <li>
                            <a href="{{ route('regulation') }}" class="header-box__navigation--nav-item">Regulamento</a>
                        </li>
                        <li>
                            <a href="{{url('cartapremiado')}}/contato" class="header-box__navigation--nav-item">Contato</a>
                        </li>
                    <li>
                        <a href="{{ route('cooperatives') }}" class="header-box__navigation--nav-item">Cooperativas participantes</a>
                    </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="btn-menu-responsive d-block d-lg-none">
            <div class="icon-bar"></div>
        </div>
    </div>
</header>