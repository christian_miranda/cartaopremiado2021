<footer class="footer-site inner">
    <div class="container">
        <div class="text-center logo-footer">
            <img src="{{ asset('site/images/logo.png') }}" alt="">
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <p class="text-justify">
                    Participação válida de 1/12/19 a 29/2/20 e exclusiva para pessoas jurídicas e físicas maiores de 18 anos, domiciliadas em território nacional, titulares dos cartões de crédito Sicoob participantes, que sejam associadas às cooperativas filiadas ao Sicoob Central Crediminas indicadas no regulamento. Para condições de participação, relação de cartões e cooperativas participantes, descrição dos prêmios, datas e horários dos períodos individuais de participação e dos sorteios, consulte o regulamento no hotsite da promoção: www.cartaopremiadosicoobcard.com.br. Certificado de Autorização SEAE nº 04.006606/2019. Imagens ilustrativas. *As compras realizadas em determinado período individual de participação serão somadas para atribuição de números da sorte e eventual saldo residual será descartado ao final do mês. **Os valores das transações realizadas pelos meios de pagamento SIPAG serão computados em dobro para atribuição de números da sorte.
                </p>
                <p class="text-center atendimento">
                    Central de Atendimento Sicoobcard <br>
                    Regiões Metropolitanas: 4007-1256 <br>
                    Demais regiões: 0800 702 0756 <br> Ouvidoria: 0800 725 0996<br>
                    Atendimento: seg. a sex. - das 8h às 20h  www.ouvidoriasicoob.com.br <br>Deficientes auditivos ou de fala: 0800 940 0458
                </p>
            </div>
        </div>
    </div>
</footer>