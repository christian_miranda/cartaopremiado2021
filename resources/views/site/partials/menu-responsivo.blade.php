<nav>
    <div class="body-menu-responsive">
        <ul class="lista-menu">
            <li>
                <a href="{{ route('home') }}" class="header-box__navigation--nav-item">Home</a></li>
            <li>
                <a class="header-box__navigation--nav-item" href="javascript:void(0);" onclick="scrollToDiv('#como_participar');">Como participar</a>
            </li>
            <li>
                <a class="header-box__navigation--nav-item" href="javascript:void(0);" onclick="scrollToDiv('#numeros_da_sorte');">Números da sorte</a>
            </li>
            <li>
                <a class="header-box__navigation--nav-item" href="javascript:void(0);" onclick="scrollToDiv('#sorteio');">Sorteios</a>
            </li>
            <li>
                <a href="{{ route('regulation') }}" class="header-box__navigation--nav-item">Regulamento</a>
            </li>
            <li>
                <a class="header-box__navigation--nav-item" href="javascript:void(0);" onclick="scrollToDiv('#contato');">Contato</a>
            </li>
            <li>
                <a href="{{ route('cooperatives') }}" class="header-box__navigation--nav-item">Cooperativas participantes</a>
            </li>
        <!-- 
            <li>
                <a href="" class="header__link"> O Sicoob </a>
            </li>
            <li>
                <a href="" class="header__link">
                    Produtos
                </a>
            </li>
            <li>
                <a href="" class="header__link">
                    Notícias
                </a>
            </li>
            <li>
                <a href="" class="header__link">
                    Faça Parte
                </a>
            </li>
            <li>
                <a href="" class="header__link">
                    pesquisar
                </a>
            </li> -->
        </ul>
    </div>
</nav>