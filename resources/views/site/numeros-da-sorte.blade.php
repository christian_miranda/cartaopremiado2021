@extends('site.layout.site')

@section('content')

	<div class="fh5co-loader"></div>
	
	<div id="page">
    @include ('site.partials.header')
    @include ('site.partials.menu-responsivo')
    @include ('site.partials.banner-principal')
    @include ('site.partials.banner-mob')
        @include("site.partials.numeros-pesquisa")
        @include("site.partials.my-numbers")
    @include ('site.partials.form-contato--mob')
    @include ('site.partials.footer')
</div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            setTimeout(function () {
                scrollToDiv('#my-numbers');
            }, 600);
        });
    </script>
@endpush