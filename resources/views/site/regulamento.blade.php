@extends('site.layout.site')

@section('content')

@include ('site.partials.header_interno')
@include ('site.partials.menu-responsivo-interno')
@include ('site.partials.banner-principal')
@include ('site.partials.banner-mob')

<div id="regulamento"></div>
<div class="" style="background-color:#42a0ab;">
	<div class="container">
			<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-10 offset-md-1" style="padding: 50px 0px 50px 0px;">
							<h1 class="h1-title"><img src="{{asset('site/images/txt_regulamento.png')}}" alt="" class="img-responsive animate-box fadeInUp animated-fast" style="width:310px;"></h1>
					</div>
			</div>
			<div class="row justify-content-center">		
							
					<?php for($i=1;$i<16;$i++){ ?>
							<div class="col-md-10 text-center">

							<img width="100%" class="img-responsive animate-box" src="{{asset('site/images/regulamento/'.$i.'.jpg')}}" alt="Regulamento">

						</div>
					<?php } ?>							
			</div>
	</div>
</div>
		@include ('site.partials.form-contato--mob')
    @include ('site.partials.footer')

@endsection

@push('scripts')
	<script type="text/javascript">
        $(document).ready(function() {
            setTimeout(function () {
                scrollToDiv('#regulamento');
            }, 600);
        });
	</script>
@endpush
