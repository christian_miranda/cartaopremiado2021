<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
    */
    public function __construct($data)
    {
        $this->data = $data;
        //dd($this->data);
    }

    /**
     * Build the message.
     *
     * @return $this
    */
    public function build()
    {

        return $this->from('christian@loggia.com.br')
                ->view('email.contato')
                ->subject('Sicoob - Mensagem Cartão Premiado')
                ->with(['data' => $this->data,]);

        // return $this->view('email.contato')
        //     ->from('email@loggia.com', 'Cartão Premiado')
        //     //->bcc('coopcompras@loggia.com.br')
        //     //->bcc('edson@loggia.com.br')
        //     ->subject('Sicoob - Cartão Premiado')
        //     ->with(['data' => $this->data]);

    }
}
